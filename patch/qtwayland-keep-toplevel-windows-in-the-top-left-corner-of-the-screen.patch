From de46b3c6d1bef4b8f64c7db7069c8fd33ef23ed9 Mon Sep 17 00:00:00 2001
From: David Redondo <qt@david-redondo.de>
Date: Wed, 8 Jun 2022 11:25:59 +0200
Subject: [PATCH] Keep toplevel windows in the top left corner of the screen

Cherry-picked from commit a46795a2. Fixes https://bugs.kde.org/show_bug.cgi?id=436016
------------------------------------------------------------------------
We can't know the actual position of a window on the screen. This causes
an issue when Widgets try to position a popup/menu absolutely and keep
it on the screen when the screen geometry doesn't include (0,0).
Instead report their positions always as the top left corner of
the screen that they are on.
This new behavior can be disabled for qt-shell or via an environment
variable by users that rely on the old behavior.

Fixes: QTBUG-85297
Change-Id: Iacb91cb03a0df87af950115760d2f41124ac06a3
Reviewed-by: Qt CI Bot <qt_ci_bot@qt-project.org>
Reviewed-by: David Edmundson <davidedmundson@kde.org>
Reviewed-by: Aleix Pol Gonzalez <aleixpol@kde.org>
---
 src/client/qwaylandintegration.cpp |  4 ++++
 src/client/qwaylandwindow.cpp      | 14 +++++++++++++-
 src/client/qwaylandwindow_p.h      |  3 +++
 3 files changed, 20 insertions(+), 1 deletion(-)

diff --git a/src/client/qwaylandintegration.cpp b/src/client/qwaylandintegration.cpp
index fbf00c6b..42e4f559 100644
--- a/src/client/qwaylandintegration.cpp
+++ b/src/client/qwaylandintegration.cpp
@@ -117,6 +117,15 @@ QWaylandIntegration::QWaylandIntegration()
         mFailed = true;
         return;
     }
+
+    const QByteArray currentDesktop = qgetenv("XDG_CURRENT_DESKTOP");
+    if (currentDesktop != QByteArray("KDE")) {
+        QWaylandWindow::fixedToplevelPositions =
+            !qEnvironmentVariableIsSet("QT_WAYLAND_DISABLE_FIXED_POSITIONS");
+    } else {
+        QWaylandWindow::fixedToplevelPositions = false;
+    }
+
 #if QT_CONFIG(clipboard)
     mClipboard.reset(new QWaylandClipboard(mDisplay.data()));
 #endif
diff --git a/src/client/qwaylandwindow.cpp b/src/client/qwaylandwindow.cpp
index ceaa4c73..ec232cd2 100644
--- a/src/client/qwaylandwindow.cpp
+++ b/src/client/qwaylandwindow.cpp
@@ -349,8 +349,13 @@ void QWaylandWindow::setGeometry_helper(const QRect &rect)
     }
 }

-void QWaylandWindow::setGeometry(const QRect &rect)
+void QWaylandWindow::setGeometry(const QRect &r)
 {
+    auto rect = r;
+    if (fixedToplevelPositions && !QPlatformWindow::parent() && window()->type() != Qt::Popup
+        && window()->type() != Qt::ToolTip) {
+        rect.moveTo(screen()->geometry().topLeft());
+    }
     setGeometry_helper(rect);

     if (window()->isVisible() && rect.isValid()) {
@@ -1023,6 +1028,13 @@ void QWaylandWindow::handleScreensChanged()

     QWindowSystemInterface::handleWindowScreenChanged(window(), newScreen->QPlatformScreen::screen());
     mLastReportedScreen = newScreen;
+    if (fixedToplevelPositions && !QPlatformWindow::parent() && window()->type() != Qt::Popup
+        && window()->type() != Qt::ToolTip
+        && geometry().topLeft() != newScreen->geometry().topLeft()) {
+        auto geometry = this->geometry();
+        geometry.moveTo(newScreen->geometry().topLeft());
+        setGeometry(geometry);
+    }

     int scale = newScreen->isPlaceholder() ? 1 : static_cast<QWaylandScreen *>(newScreen)->scale();
     if (scale != mScale) {
diff --git a/src/client/qwaylandwindow_p.h b/src/client/qwaylandwindow_p.h
index cb9135f6..1907f10d 100644
--- a/src/client/qwaylandwindow_p.h
+++ b/src/client/qwaylandwindow_p.h
@@ -98,6 +98,9 @@ public:
     QWaylandWindow(QWindow *window, QWaylandDisplay *display);
     ~QWaylandWindow() override;

+    // Keep Toplevels position on the top left corner of their screen
+    static inline bool fixedToplevelPositions = true;
+
     virtual WindowType windowType() const = 0;
     virtual void ensureSize();
     WId winId() const override;
--
GitLab
